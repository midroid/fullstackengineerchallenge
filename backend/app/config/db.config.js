module.exports = {
    HOST: "localhost",
    USER: "ppccuser",
    PASSWORD: "password",
    DB: "ppccdb",
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0,
        idle: 3000,
        acquire: 10000
    }
};
