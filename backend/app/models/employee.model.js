module.exports = (sequelize, Sequelize) => {
    const Employee = sequelize.define("employee", {
        name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        rating: {
            type: Sequelize.INTEGER
        },
        role: {
            type: Sequelize.INTEGER
        }
    });

    return Employee;
};
