module.exports = (sequelize, Sequelize) => {
    const Review = sequelize.define("review", {
        revieweeId: {
            type: Sequelize.INTEGER
        },
        reviewerId: {
            type: Sequelize.INTEGER
        },
        comment: {
            type: Sequelize.STRING
        },
        rating: {
            type: Sequelize.STRING
        },
        completed: {
            type: Sequelize.BOOLEAN
        }
    });

    return Review;
}