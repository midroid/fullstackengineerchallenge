const db = require("../models");
const Reviews = db.reviews;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  if (!req.body.reviewerId || !req.body.revieweeId) {
    res.status(400).send({
      message: "Reviewer and reviewee Id cannot be empty"
    });
    return;
  }

  const review = {
    revieweeId: req.body.revieweeId,
    reviewerId: req.body.reviewerId,
    comment: '',
    rating: 0,
    completed: false,
  }

  Reviews.create(review)
    .then(data => {
      res.send(data)
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occured"
      });
    });
};

exports.findAll = (req, res) => {
  Reviews.findAll({ where: null})
    .then(data => {
      res.send(data)
    })
    .catch(err => {
      res.status(500).send({
        message:
        err.message || "some error occured"
      });
    });
};

exports.findReviewByReviewId = (req, res) => {
  const id = req.params.id;

  Reviews.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Review with id=" + id
      });
    });
};

exports.submitReview = async (req, res) => {
  const id = req.params.id;

  Reviews.findByPk(id)
    .then(data => {
      if (data.completed) {
        res.send({
          message: "Review already submitted"
        });
        return;
      }

      const review = {
        revieweeId: data.revieweeId,
        reviewerId: data.reviewerId,
        comment: req.body.comment,
        rating: req.body.rating,
        completed: true,
      }

      Reviews.update(review, {
        where: { id: id }
      })
        .then(num => {
          if (num == 1) {
            res.send({
              message: "Review was updated successfully."
            });
          } else {
            res.send({
              message: `Cannot update Review with id=${id}.`
            });
          }
        })
        .catch(err => {
          res.status(500).send({
            message: "Error updating Review with id=" + id
          });
        });

    })
    .catch(err => {
      res.status(500).send({
        message: "Error occured"
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Reviews.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Review record was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Review with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Review with id=" + id
      });
    });
};

exports.deleteAll = (req, res) => {
  Reviews.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({message: `${nums} Reviews were deleted successfully!` })
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occured"
      })
    })
}
