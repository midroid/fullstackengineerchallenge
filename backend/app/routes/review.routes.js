module.exports = app => {
    const reviews = require("../controllers/review.controller");

    var router = require("express").Router();

    router.post("/", reviews.create);

    router.get("/", reviews.findAll);

    router.get("/:id", reviews.findReviewByReviewId);

    // router.get("/reviewer/:id", reviews.findReviewsByReviewerId);

    // router.get("/reviewee/:id", reviews.findReviewsByRevieweeId);

    router.put("/:id", reviews.submitReview);

    router.delete("/:id", reviews.delete);

    // router.delete("/:id", reviews.deleteReviewForEmployee);

    router.delete("/", reviews.deleteAll);

    app.use('/api/review', router);
};
