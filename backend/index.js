const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
var router = express.Router()

const db = require("./app/models");
db.sequelize.sync();

// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });

const corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

router.use(function (req, res, next) {
  console.log('Time:', Date.now());
  next();
});

app.use('/', router);

app.get("/", (req, res) => {
    res.json({message: "Express App Working"});
});

const employeeRoute = require("./app/routes/employee.routes");
const reviewRoute = require("./app/routes/review.routes");

employeeRoute(app);
reviewRoute(app);

const PORT = process.env.port || 8080;
app.listen(PORT, () => {
    console.log("server started");
});
